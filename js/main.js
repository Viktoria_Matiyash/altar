//animation h1
var init = function(){
    var isMobile = navigator.userAgent &&
        navigator.userAgent.toLowerCase().indexOf('mobile') >= 0;
    var isSmall = window.innerWidth < 1000;
   
    var ps = new ParticleSlider({
        ptlGap: isMobile || isSmall ? 3 : 0,
        ptlSize: isMobile || isSmall ? 3 : 1,
        width: 1e9,
        height: 1e9
    });

    var gui = new dat.GUI();
    gui.add(ps, 'ptlGap').min(0).max(5).step(1).onChange(function(){
        ps.init(true);
    });
    gui.add(ps, 'ptlSize').min(1).max(5).step(1).onChange(function(){
        ps.init(true);
    });
    gui.add(ps, 'restless');
    gui.addColor(ps, 'color').onChange(function(value){
        ps.monochrome = true;
        ps.setColor(value);
        ps.init(true);
    });

    (window.addEventListener
        ? window.addEventListener('click', function(){ps.init(true)}, false)
        : window.onclick = function(){ps.init(true)});
};

var initParticleSlider = function(){
    var psScript = document.createElement('script');
    (psScript.addEventListener
        ? psScript.addEventListener('load', init, false)
        : psScript.onload = init);
    psScript.src = 'https://s3-us-west-2.amazonaws.com/s.cdpn.io/23500/ps-0.9.js';
    psScript.setAttribute('type', 'text/javascript');
    document.body.appendChild(psScript);
};

(window.addEventListener
    ? window.addEventListener('load', initParticleSlider, false)
    : window.onload = initParticleSlider);

// active link in header
$(function () {
    $(".header__link").each(function () {
        window.location.href == this.href && $(this).addClass("active")
    })
});

// adaptive menu
$(".adaptive-menu__open-menu").click(() => {
    $(".adaptive-menu__full-menu").slideDown()
});
$(".adaptive-menu__close-menu").click(() => {
    $(".adaptive-menu__full-menu").slideUp()
});
$(".adaptive-menu__link").click(() => {
    $(".adaptive-menu__full-menu").slideUp()
});

// drop down menu
if(document.documentElement.clientWidth < 579) {
    $(".header__right-block").click(() => {
        $(".header__icons").toggle(200);
    });
    $('.header__search--adaptive').click(() => {
        $(".search").toggle(200);
    })
}

//popup
$(".js-basket").click((e) => {
    e.preventDefault();

    let order = $(e.target).data().order;

    if (typeof order != "undefined") {
        $.ajax({
            type: 'get',
            url: '/order',
            data: "i=" + order,
            dataType: "html",
            success: function (data) {
                $('.basket').html(data);
            },
            error: function (error) {
                alert('Ошибка соединения с сервером, попробуйте отправить заявку позже.');
            }
        });
    } else if ($(".basket").children().length == 0) return;
    
    $(".basket__overlay").fadeIn(400, () => {
        $(".basket").css("display", "block").animate({opacity: 1, top: "50%"}, 200)
    });
});

$(".basket__close, .basket__overlay").click(() => {
    $(".basket").animate({opacity: 0, top: "45%"}, 200, function () {
        $(this).css("display", "none"), $(".basket__overlay").fadeOut(400)
    })
});

//slider
$(".js-slider").slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    infinite: true,
    speed: 400,
    nav: true,
    dots: false,
    autoplaySpeed: 2000,
    pauseOnHover: true,
    responsive:[{
        breakpoint:1080,
        settings:{
            slidesToShow:2,
            slidesToScroll:1,
            dots: false,
            nav: false,
            margin: 0
        }},
        {
            breakpoint:766,
            settings:{
                slidesToShow:1,
                slidesToScroll:1,
                dots: false,
                nav: false,
                margin: 0
        }}]
});

//ajax
function formSend() {
    $('.subscription__form .clear').val('');
    $('.subscription__notification').show(500);
}
$(".subscription__form").on('submit', function (e) {
    e.preventDefault();
    $.ajax({
        type: 'get',
        url: '/submit',
        data: $(this).serialize(),
        dataType: "json",
        success: function (data) {
            if (data.status == true) {
                formSend();
            } else {
                alert(data.error_message);
            }
        },
        error: function (error) {
            alert('Ошибка соединения с сервером, попробуйте отправить заявку позже.');
        }
    });
});

//accordion
const $uiAccordion = $('.help__accordion');
$uiAccordion.accordion({
    collapsible: true,
    heightStyle: 'content',
    activate: (event, ui) => {
        const newHeaderId = ui.newHeader.attr('id');

        if (newHeaderId) {
            history.pushState(null, null, `#${newHeaderId}`);
        }
    },
    create: (event, ui) => {
        const $this = $(event.target);
        const $activeAccordion = $(window.location.hash);

        if ($this.find($activeAccordion).length) {
            $this.accordion('option', 'active', $this.find($this.accordion('option', 'header')).index($activeAccordion));
        }
    }
});
$(window).on('hashchange', event => {
    const $activeAccordion = $(window.location.hash);
    const $parentAccordion = $activeAccordion.parents('.help__accordion')

    if ($activeAccordion.length) {
        $parentAccordion.accordion('option', 'active', $parentAccordion.find($uiAccordion.accordion('option', 'header')).index($activeAccordion));
    }
});

on_more = function (e) {
    e.preventDefault();
    let parent = $(this).parent();
    $.ajax({
        type: 'GET',
        url: '/category/more',
        data: $(this).attr('data-category'),
        dataType: "text",
        success: function (data) {
                parent.html(data);
                $(".subcategory__more:not(.ajax_bound)").addClass('ajax_bound').on('click', on_more);
        },
        error: function (error) { console.log(error); }
    });
};
$(".subcategory__more:not(.ajax_bound)").addClass('ajax_bound').on('click', on_more);

function send_request(request) {
    let xhttp = new XMLHttpRequest();
    xhttp.open("GET", request, true);
    xhttp.send();
}

function get_order(checkout) {

    let idx = checkout.href.lastIndexOf("=");
    return checkout.href.substr(idx + 1).split(":");
}

function cart_delete(id, value, element) {
    let product = element.parentElement.parentElement;
    $(product).hide("slow", function(){ product.remove(); });

    let checkout = $(".basket__next")[0];
    let order = get_order(checkout);

    count = 0;
    for (i = 0; i < order.length; i += 2) {
        if (order[i] == id) {
            order[i] = 0;
            count = parseInt(order[i + 1], 10);
            order[i + 1] = 0;
            break;
        }
    }

    send_request("/order?i=" + id + "&o=clear");

    new_ref = "/checkout?i=";
    let first = true;
    
    for (i = 0; i < order.length; i++) {
        if (order[i] > 0) {
            if (first) {
                first = false;
            } else {
                new_ref += ':';
            }

            new_ref += order[i];
        }
    }

    checkout.href = new_ref;

    let label = $(".cart_amount_value")[0];
    let amount = parseInt(label.innerText, 10);
    label.innerText = amount - count * value;
    
    let order_count_labels = $('.order__count');
    if (typeof order_count_labels !== 'undefined' && order_count_labels.length > 0) {
        let current_count = parseInt(order_count_labels[0].innerText);
        let price = parseInt($('.order_price_label')[0].innerText);
        let products_price = parseInt($('.products_price_label')[0].innerText);
        let new_count = current_count - count;
        order_count_labels[0].innerText = new_count;
        $('.order_price_label')[0].innerText = price - value * count;
        $('.products_price_label')[0].innerText = products_price - value * count;

        if (new_count == 0) {
            $('.product_label')[0].innerText = 'товаров';
            $(".order__send").attr("disabled", "disabled");
        } else {
            if (new_count == 1)     $('.product_label')[0].innerText = 'товар';
            else if (new_count < 5) $('.product_label')[0].innerText = 'товара';
            else                    $('.product_label')[0].innerText = 'товаров';
        }
    }
}

function update_price(id, value, range) {
    if (value < 0 && range.value == 1) return;
    if (value > 0 && range.value == range.max) return;
    
    let checkout = $(".basket__next")[0];
    let order = get_order(checkout);

    for (i = 0; i < order.length; i += 2) {
        if (order[i] == id) {
            order[i + 1] = parseInt(order[i + 1], 10) + (value > 0 ? 1 : -1);
            break;
        }
    }

    send_request("/order?i=" + id + "&o=" + (value > 0 ? "add" : "remove"));

    new_ref = "/checkout?i=";
    let first = true;

    for (i = 0; i < order.length; i++) {
        if (first) {
            first = false;
        } else {
            new_ref += ':';
        }

        new_ref += order[i];
    }

    checkout.href = new_ref;

    let label = $(".cart_amount_value")[0];
    let amount = parseInt(label.innerText, 10);
    label.innerText = amount + value;

    let order_count_labels = $('.order__count');
    if (typeof order_count_labels !== 'undefined' && order_count_labels.length > 0) {
        let count = parseInt(order_count_labels[0].innerText);
        let price = parseInt($('.order_price_label')[0].innerText);
        let products_price = parseInt($('.products_price_label')[0].innerText);
        let new_count = count + (value > 0 ? 1 : -1);
        order_count_labels[0].innerText = new_count;
        $('.order_price_label')[0].innerText = price + value;
        $('.products_price_label')[0].innerText = products_price + value;

        if (new_count == 1)     $('.product_label')[0].innerText = 'товар';
        else if (new_count < 5) $('.product_label')[0].innerText = 'товара';
        else                    $('.product_label')[0].innerText = 'товаров';
    }
}

//gallery in product
$(".product__small-photo").on("click", "a", function () {
    $(this).addClass("current").siblings().removeClass("current");
    $(".product__img").attr("src", $(this).prop("href"));
    return false;
});

//count in basket
$(".basket__but").bind('click', () => {
    let count = $('.basket__number').val();
});

//tabs in product
$(".tabs__item").not(":first").hide();
$(".tabs__title").click(function() {
    $(".tabs__title").removeClass("active").eq($(this).index()).addClass("active");
    $(".tabs__item").hide().eq($(this).index()).fadeIn()
}).eq(0).addClass("active");

//tabs in account
$(".account__title").click(function() {
    $(".account__title").removeClass("active").eq($(this).index()).addClass("active");
    $(".account__item").hide().eq($(this).index()).fadeIn()
});

//slider in product
$('.js-slider-review').slick({
    dots: false,
    infinite: true,
    speed: 500,
    autoplay:100,
    // fade: true,
    cssEase: 'linear'
});

//content radiobutton
$('.order__checkbox-item').on('change', function () {
    if (typeof $(this).data('fee') !== 'undefined') {
        let delivery_fee = parseInt($(this).data('fee'));
        let old_fee = parseInt($('.delivery_fee_label')[0].innerText);
        let price = parseInt($('.order_price_label')[0].innerText);

        $('.delivery_fee_label')[0].innerText = delivery_fee;
        $('.order_price_label')[0].innerText = price - old_fee + delivery_fee;
    }

    if($('#pickup').prop('checked')){
        $('.order__pickup').css('display', 'block');
        $('.order__postoffice').css('display', 'none');
        $('.order__courier').css('display', 'none');
    }
    else if (($('#postoffice').prop('checked'))) {
        $('.order__postoffice').css('display', 'block');
        $('.order__pickup').css('display', 'none');
        $('.order__courier').css('display', 'none');
    }
    else if (($('#courier').prop('checked'))) {
        $('.order__courier').css('display', 'block');
        $('.order__pickup').css('display', 'none');
        $('.order__postoffice').css('display', 'none');
    }
});


//login form
// $(".login__form").on('submit', function (e) {
//     e.preventDefault();
//     $.ajax({
//         type: 'GET',
//         url: '',
//         data: $(this).serialize(),
//         dataType: "json",
//         success: function (data) {
//             if (data.status == true) {
//                 document.location.href = 'account.html';
//             } else {
//                 alert(data.error_message);
//             }
//         },
//         error: function (error) {
//             alert('Ошибка соединения с сервером, попробуйте войти чуть позже.');
//         }
//     });
// });

// $(".registration__send").click(function () {
//     $(".error").hide();
//     var valueX = $(".registration__pass").val();
//     var valueY = $(".registration__pass--another").val();
//     if (valueX !== valueY) {
//         $('.registration__error--pass').show();
//     }
// });